<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
    <div class="wrapper">
        <div class="wrapper-flex">
            <div class="header-main">
                <div class="header-main-flex">
                    <div class="language">
                        <div class="language-flex">
                            <span class="en">en</span>
                            <span class="ua">ua</span>
                            <span class="ru">ru</span>
                        </div>
                    </div>
                    <div class="social-networks">
                        <div class="social-networks-flex">
                            <a href="#"><span class="insta">I</span></a>
                            <a href="#"><span class="fb">F</span></a>
                            <a href="#"><span class="google">G</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-second">
                <div class="header-second-flex">
                    <div class="menu">
                        <div class="menu-flex">
                            <a href="#"><span>Menu</span></a>
                            <a href="#"><span>Photo</span></a>
                            <a href="#"><span>Content</span></a>
                            <a href="#"><span>Location</span></a>
                            <a href="#"><span>Help</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>